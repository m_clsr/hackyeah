import os
import sys
sys.path.insert(0, os.path.abspath('../'))

from app.app import app
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
