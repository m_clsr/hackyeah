import requests
from time import sleep
import sys
from subprocess import Popen, PIPE
from time import time

while True:
    url = 'http://3d4all.com.pl'
    r = requests.post(url+'/printer/check', data={'printer_id': sys.argv[1]})
    print('request sent')
    if r.content != b'no job':
        job_id = r.json()['job_id']
        g_code = r.json()['gcode']
        with open('to_print.gco', 'w') as file:
            file.write(g_code)
        command = 'python /home/mat/Printrun/printcore.py -s -b 250000 /dev/ttyUSB0 to_print.gco'
        with Popen(command, stdout=PIPE, universal_newlines=True, shell=True, executable='/bin/bash') as p:
            for line in p.stdout:
                print(line, end='')  # process line here
                message = line.split(': ')
                if message[0] == "Printing":
                    print('sending request with print')
                    requests.post(url+'/printer/confirm', data={'printer_id': sys.argv[1], 'job_id': job_id, 'status':message[0]} )

                elif message[0] == "Progress":
                    print('sending request with progress ' + str(time()) )
                    requests.post(url+'/printer/update', data={'printer_id': sys.argv[1], 'job_id': job_id, 'progress': message[1]})
                p.stdout.flush()
        requests.post(url + '/printer/update', data={'printer_id': sys.argv[1], 'job_id': job_id, 'progress': '100%'})
        requests.post(url+'/printer/confirm', data={'printer_id': sys.argv[1], 'job_id': job_id, 'status': 'Done'})
    sleep(5)
