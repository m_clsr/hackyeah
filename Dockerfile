FROM python:3.5.2
RUN pip install pipenv
COPY . /hackyeah
WORKDIR /hackyeah
RUN pipenv install

CMD ["pipenv", "run", "gunicorn", "-w 3", "-b :8080", "run:app"]
