from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import ForeignKey
from sqlalchemy import Column, Integer, String
from app import db
from flask_login import UserMixin


class Users(db.Model, UserMixin):
    __tablename__ = 'Users'

    user_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(30))

    def __init__(self, name=None, password=None, email=None):
        self.name = name
        self.password = password
        self.email = email

    def get_id(self):
        return self.user_id


class Jobs(db.Model):
    __tablename__ = 'Jobs'

    job_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey(Users.user_id, onupdate='CASCADE', ondelete='CASCADE'))
    printer_id = db.Column(db.Integer)
    date = db.Column(db.Float)
    file_path = db.Column(db.String(255))
    status = db.Column(db.String(255))
    progress = db.Column(db.String)
    message = db.Column(db.String)
    price = db.Column(db.Float)

    def __init__(self, user_id, date, file_path, message, status='Pending', progress='0%', printer_id=1, price=0.0):
        self.printer_id = printer_id
        self.user_id = user_id
        self.date = date
        self.file_path = file_path
        self.status = status
        self.message = message
        self.progress = progress
        self.price = price
