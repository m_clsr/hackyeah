import sys
import re


def gcode_evaluation(path_to_file):
    extrude = re.compile(r'(?<=E)\d+\.?\d*')
    extrusion_length = 0

    with open(path_to_file, 'r') as gcode_file:
        lengths = [float(item) for item in re.findall(extrude, gcode_file.read())]
        current_difference = 0
        current = 0
        for i, value in enumerate(lengths):
            if value == 0:
                extrusion_length += current_difference
                current = value
                current_difference = -1
            elif i > 0:
                previous = current
                current = value
                if current - previous > 0:
                    current_difference += (current - previous)

    return extrusion_length
