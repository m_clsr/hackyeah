#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import render_template, request, redirect, url_for, jsonify
from flask_login import LoginManager, login_required, logout_user, login_user, current_user
import logging
from logging import Formatter, FileHandler
from .forms import *
import time
import os
import json
import glob
from uuid import uuid4
from app import app
from app import db
from .models import Jobs, Users
from werkzeug.datastructures import ImmutableOrderedMultiDict
import requests
from app.gcode_eval import gcode_evaluation


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

db.create_all()


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(Users).filter_by(user_id=user_id).first()

#----------------------------------------------------------------------------#
# Controllers.
#----------------------------------------------------------------------------#


@app.route('/')
def home():
    return render_template('pages/placeholder.home.html')


@app.route('/about')
def about():
    return render_template('pages/placeholder.about.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.query(Users).filter_by(name=form.name.data).first()
        if user and user.password == form.password.data:
            login_user(user)
            return redirect(url_for('show_dashboard'))
        else:
            return 'Wrong credentials'
    return render_template('forms/login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        new_user = Users(name=form.name.data, email=form.email.data, password=form.password.data)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))
    else:
        return render_template('forms/register.html', form=form)


@app.route('/index')
@login_required
def index():
    return render_template('pages/upload.loggedin.html')


@app.route('/dashboard')
def show_dashboard():
    try:
        jobs = db.session.query(Jobs).filter(Jobs.user_id == current_user.user_id).order_by(Jobs.status).all()
        for item in jobs:
            item.date= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item.date))
        return render_template('pages/dashboard.html', jobs=jobs)
    except AttributeError:
        return redirect(url_for('login'))

# upload files.


@app.route("/upload", methods=["POST"])
@login_required
def upload():
    """Handle the upload of a file."""
    form = request.form

    # Create a unique "session ID" for this particular batch of uploads.
    upload_key = str(uuid4())

    # Is the upload using Ajax, or a direct POST by the form?
    is_ajax = False
    if form.get("__ajax", None) == "true":
        is_ajax = True

    # Target folder for these uploads.
    target = "app/static/uploads/{}".format(upload_key)
    try:
        os.mkdir(target)
    except:
        if is_ajax:
            return ajax_response(False, "Couldn't create upload directory: {}".format(target))
        else:
            return "Couldn't create upload directory: {}".format(target)

    print("=== Form Data ===")
    for key, value in list(form.items()):
        print(key, "=>", value)

    for upload in request.files.getlist("file"):
        filename = upload.filename.rsplit("/")[0]
        destination = "/".join([target, filename])
        print("Accept incoming file:", filename)
        print("Save it to:", destination)
        upload.save(destination)
        price = 2 + gcode_evaluation(path_to_file=destination) / 2000
    new_job = Jobs(current_user.user_id, time.time(), destination, request.form['textarea'], price=price)
    db.session.add(new_job)
    db.session.commit()

    if is_ajax:
        return ajax_response(True, upload_key)
    else:
        return redirect(url_for("upload_complete", uuid=upload_key))


@app.route("/files/<uuid>")
def upload_complete(uuid):
    """The location we send them to at the end of the upload."""

    # Get their files.
    root = "app/static/uploads/{}".format(uuid)
    if not os.path.isdir(root):
        return "Error: UUID not found!"

    files = []
    for file in glob.glob("{}/*.*".format(root)):
        fname = file.split(os.sep)[-1]
        files.append(fname)
    return render_template("pages/files.html",
                           uuid=uuid,
                           files=files,
                           )


def ajax_response(status, msg):
    status_code = "ok" if status else "error"
    return json.dumps(dict(
        status=status_code,
        msg=msg,
    ))

# API for printer


@app.route('/printer/check', methods=['POST'])
def check_for_jobs():
    job = db.session.query(Jobs).filter(Jobs.printer_id == request.form['printer_id']).filter(Jobs.status == 'Paid').first()
    if job:
        with open(job.file_path, 'r') as file:
            gcode = file.read()
            resp = {'job_id': job.job_id, 'gcode': gcode}
            return jsonify(resp)
    else:
        return 'no job'


@app.route('/printer/confirm', methods=['POST'])
def confitm_job():
    job = db.session.query(Jobs).filter(Jobs.job_id == request.form['job_id']).first()
    job.status = request.form['status']
    db.session.commit()
    return 'OK'



@app.route('/printer/update', methods=['POST'])
def update_job():
    job = db.session.query(Jobs).filter(Jobs.job_id == request.form['job_id']).first()
    job.progress = request.form['progress']
    db.session.commit()
    return 'OK'

#
# paypal
#


@app.route('/success/<int:job_id>')
def success(job_id):
    try:
        job = db.session.query(Jobs).filter(Jobs.job_id == job_id).first()
        job.status = "Paid"
        db.session.commit()
        return render_template("pages/success.html")
    except Exception as e:
        return(str(e))


@app.route('/purchase/<int:job_id>')
def purchase(job_id):
    try:
        job = db.session.query(Jobs).filter(Jobs.job_id == job_id).first()
        return render_template("pages/subscribe.html", current_user=current_user, price=job.price, job_id=job_id)
    except Exception as e:
        return(str(e))


@app.route('/ipn/', methods=['POST'])
def ipn():
    try:
        arg = ''
        request.parameter_storage_class = ImmutableOrderedMultiDict
        values = request.form
        for x, y in values.iteritems():
            arg += "&{x}={y}".format(x=x, y=y)

        validate_url = 'https://www.sandbox.paypal.com' \
                       '/cgi-bin/webscr?cmd=_notify-validate{arg}' \
                       .format(arg=arg)
        r = requests.get(validate_url)
        request.form.get('item_number')
        print("1")
        if r.text == 'VERIFIED':
            job = db.session.query(Jobs).filter(Jobs.job_id == request.form.get('item_number')).first()
            job.status = "Paid"
            db.session.commit()
            print("2")
            # Change in database status for paid
            #
        else:
            return redirect(url_for('show_dashboard'))
        return r.text
    except Exception as e:
        return str(e)

# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    # db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')


if __name__ == '__main__':
    app.run()

# Or specify port manually:
'''
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
'''
