from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
app.config.from_object('app.config')
db = SQLAlchemy(app)
app.wsgi_app = ProxyFix(app.wsgi_app)
